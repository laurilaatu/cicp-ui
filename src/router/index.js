import Vue from 'vue'
import Router from 'vue-router'
import CicpHeader from '@/components/CicpHeader'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'CicpHeader',
      component: CicpHeader
    }
  ]
})
